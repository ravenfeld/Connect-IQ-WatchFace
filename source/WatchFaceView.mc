using Toybox.WatchUi as Ui;
using Toybox.Time.Gregorian as Gregorian;
using Toybox.Activity as Act;
using Toybox.System as Sys;
using Toybox.Graphics as Gfx;
using Toybox.Application as App;
using Toybox.Time;

class WatchFaceView extends Ui.WatchFace{
	hidden var cx;
    hidden var cy;
    hidden var settings;
    hidden var active;
    hidden var text_width_hour_10;
    hidden var text_width_point;
	hidden var text_width_minute;
	hidden var text_width_second;
	hidden var text_height_hour;
	hidden var text_height_second;
	hidden var text_y_second;
	hidden var start_x_active_hour_10;
	hidden var start_x_sleep_hour_10;
	hidden var text_color;
	hidden var shade_color;
	hidden var calorie_icon_white;
	hidden var calorie_icon_black;
	hidden var step_icon_white;
	hidden var step_icon_black;
	hidden var distance_icon_white;
	hidden var distance_icon_black;
	hidden var heart_icon_white;
	hidden var heart_icon_black;
	hidden var sunrise_icon_white;
	hidden var sunrise_icon_black;
	hidden var alarm_icon_white;
	hidden var alarm_icon_black;
	hidden var change_color;
	hidden var time_color;
	hidden var altitude = 0;
	hidden var heart_rate = 0;
	hidden var lastLoc;
	hidden var sunset_sunrise;
	hidden var sunset;
	hidden var arc_type;
	hidden var info_top;
	hidden var info_bottom;
	hidden var display_second;
	hidden var date_type;
	hidden var battery_low;
	hidden var battery_percentage;
	hidden var battery_profile;
	hidden var altitude_profile;
	hidden var altitude_step;
	hidden var arc_width;
	hidden var alarm;
	hidden var style_battery;
	hidden var minute_change_color;
	hidden var hour_color;
	hidden var minute_color;
	hidden var bgk_color;
	hidden var arc_color_goal;
	hidden var second_color;
	hidden var arc_color;
	hidden var arc_color_low_battery;
	hidden var shade_color_property;
	hidden var hour_color_property;
	hidden var minute_color_property;
	hidden var bgk_color_property;
	hidden var arc_color_goal_property;
	hidden var arc_color_low_battery_property;
	hidden var arc_color_property;
	hidden var second_color_property;
	hidden var largeFont;
	hidden var smallFont;
		
	function initialize() {
        WatchFace.initialize();
        calorie_icon_white = Ui.loadResource(Rez.Drawables.CalorieIconWhite);
    	calorie_icon_black = Ui.loadResource(Rez.Drawables.CalorieIconBlack);

    	step_icon_white = Ui.loadResource(Rez.Drawables.StepIconWhite);
    	step_icon_black = Ui.loadResource(Rez.Drawables.StepIconBlack);
    	
    	distance_icon_white = Ui.loadResource(Rez.Drawables.DistanceIconWhite);
    	distance_icon_black = Ui.loadResource(Rez.Drawables.DistanceIconBlack);
    	
    	heart_icon_white = Ui.loadResource(Rez.Drawables.HeartIconWhite);
    	heart_icon_black = Ui.loadResource(Rez.Drawables.HeartIconBlack);
    	    	
    	sunrise_icon_white = Ui.loadResource(Rez.Drawables.SunriseIconWhite);
    	sunrise_icon_black = Ui.loadResource(Rez.Drawables.SunriseIconBlack);
    	
    	alarm_icon_white = Ui.loadResource(Rez.Drawables.AlarmIconWhite);
    	alarm_icon_black = Ui.loadResource(Rez.Drawables.AlarmIconBlack);
    	
    	largeFont =  Ui.loadResource(Rez.Fonts.LargeFont);
    	smallFont =  Ui.loadResource(Rez.Fonts.SmallFont);
    	
    	//Property
    	arc_type =  App.getApp().getProperty("arc_type");
    	info_top = App.getApp().getProperty("info_top");
    	info_bottom = App.getApp().getProperty("info_bottom");
    	display_second = App.getApp().getProperty("second_display");
    	date_type =  App.getApp().getProperty("date_type");
    	battery_low =  App.getApp().getProperty("battery_low").toNumber();
    	battery_percentage = App.getApp().getProperty("battery_percentage");
    	battery_profile = App.getApp().getProperty("battery_profile");
    	altitude_profile = App.getApp().getProperty("altitude_profile");
    	altitude_step =  App.getApp().getProperty("altitude_step").toNumber();
    	arc_width =  App.getApp().getProperty("arc_width").toNumber();
    	alarm =  App.getApp().getProperty("alarm_clock_display");
    	style_battery = App.getApp().getProperty("style_battery").toNumber();
    	
    	shade_color_property =  App.getApp().getProperty("shade_color");
    	minute_change_color =  App.getApp().getProperty("minute_change_color").toNumber();
    	hour_color_property = App.getApp().getProperty("hour_color");
    	minute_color_property = App.getApp().getProperty("minute_color");
    	bgk_color_property =  App.getApp().getProperty("bgk_color");
    	arc_color_goal_property  = App.getApp().getProperty("arc_color_goal");
    	arc_color_low_battery_property = App.getApp().getProperty("arc_color_low_battery");
    	arc_color_property = App.getApp().getProperty("arc_color");
    	second_color_property = App.getApp().getProperty("second_color");
    	  	
    }
        
    function onExitSleep() {
    	active=true;
    	Ui.requestUpdate();
    }

    function onEnterSleep() {
    	active=false;
    	Ui.requestUpdate();
    }
    
    function onShow(){
		settings = System.getDeviceSettings();
    }
    
    function onHide(){
    	
    }
    
    function onLayout(dc) {
        cx = dc.getWidth() / 2;
        cy = dc.getHeight() / 2;
        
        text_width_hour_10 = dc.getTextWidthInPixels("88",largeFont);
        text_width_point = dc.getTextWidthInPixels(":",largeFont);
        text_height_hour = dc.getFontHeight(largeFont);
        text_width_minute = dc.getTextWidthInPixels("88",largeFont);
        text_width_second=dc.getTextWidthInPixels("88",smallFont);
        text_height_second = dc.getFontHeight(smallFont);
        
        start_x_active_hour_10=(dc.getWidth()-(text_width_hour_10+text_width_point+text_width_minute+text_width_second+8))/2;
        start_x_sleep_hour_10=(dc.getWidth()-(text_width_hour_10+text_width_point+text_width_minute+4))/2;
   
    }
    
    function onUpdate(dc) {
    	var actInfo = Act.getActivityInfo();
    	
    	//Battery
    	var battery = Sys.getSystemStats().battery;
    	//Hour
        var moment = Time.now();
        var info_date = Gregorian.info(moment, Time.FORMAT_LONG);
        //Altimeter
        
		if (actInfo != null && actInfo.altitude != null) {
			altitude = actInfo.altitude;
			var metric = settings.elevationUnits;
			if (metric==Sys.UNIT_STATUTE) {
				altitude = altitude*3.38;
			}				
		}
    	//hr
		if(info_top == 6 || info_bottom == 7){
			if(ActivityMonitor has :HeartRateIterator) {
    			var hrIter = ActivityMonitor.getHeartRateHistory(null, true);
        		if(hrIter != null){
        			var hr = hrIter.next();
					heart_rate = (hr.heartRate != ActivityMonitor.INVALID_HR_SAMPLE && hr.heartRate > 0) ? hr.heartRate : 0; 		
    			}
    		}
    	}
    	    	
    	//sunset or sunrise
    	if(actInfo.currentLocation!=null && (info_top == 7 || info_bottom == 8)){
    		lastLoc = actInfo.currentLocation.toRadians();
    		var sunrise_moment = getMoment(moment,SUNRISE);
    		var sunset_moment = getMoment(moment,SUNSET);

    		if(moment.greaterThan(sunset_moment) || moment.lessThan(sunrise_moment)){
    			sunset_sunrise = momentToString(sunrise_moment);
    			sunset=false;
    		}else{
    			sunset_sunrise = momentToString(sunset_moment);
    			sunset=true;
    		}	
    	}else{
    		sunset_sunrise = Ui.loadResource(Rez.Strings.none);
    	}
    	
    
    	if(altitude_profile && altitude>altitude_step){
			info_bottom=0;
		}
    	   	
    	var bgk_color = getBackgroundColor();
    	dc.setColor(bgk_color,bgk_color);
    	
    	if(bgk_color==Gfx.COLOR_BLACK){
    		text_color=Gfx.COLOR_WHITE;
    	}else{
    		text_color=Gfx.COLOR_BLACK;
    	}
    	
    	shade_color = getColorShade(moment);
        dc.clear();
        		
        drawHour(dc,info_date,info_bottom,active&&display_second);
    
		var y = dc.getHeight()/6;
		
		if((battery_profile && battery<=battery_low) || style_battery > 0){
    		y = dc.getHeight()*0.19;
    		if(style_battery>0){
    			Battery.drawIcon(dc,battery,battery_low,cx,dc.getHeight()*0.07,text_color,style_battery);
    		}else{
    			if(battery_percentage){
    				Battery.drawIcon(dc,battery,battery_low,cx,dc.getHeight()*0.07,text_color,2);
    			}else{
    				Battery.drawIcon(dc,battery,battery_low,cx,dc.getHeight()*0.07,text_color,1);
    			}
    		}
    	}
		
		if(info_top == 0){
			Date.drawDate(dc,info_date,cx,y,text_color,date_type);
		}else if( info_top == 1){
			if(battery_percentage){
				Battery.drawIcon(dc,battery,battery_low,cx,y,text_color,2);
			}else{
				Battery.drawIcon(dc,battery,battery_low,cx,y,text_color,1);
			}
		}else if (info_top == 2 && settings.phoneConnected){
			PhoneConnected.drawIcon(dc,cx,y,text_color);
		}else if(info_top == 3){
			var calorie_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				calorie_icon = calorie_icon_white;
			}else{
				calorie_icon = calorie_icon_black;
			}
			Utils.drawIconText(dc,ActivityMonitor.getInfo().calories,cx,y,text_color,calorie_icon);
		}else if(info_top == 4){
			var step_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				step_icon = step_icon_white;
			}else{
				step_icon = step_icon_black;
			}
			Utils.drawIconText(dc,ActivityMonitor.getInfo().steps,cx,y,text_color,step_icon);
		}else if(info_top == 5){
			var distance_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				distance_icon = distance_icon_white;
			}else{
				distance_icon = distance_icon_black;
			}
		
			InfoMonitor.drawIconDistance(dc,ActivityMonitor.getInfo().distance,cx,y,text_color,distance_icon);
		}else if(info_top == 6){
			var heart_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				heart_icon = heart_icon_white;
			}else{
				heart_icon = heart_icon_black;
			}
		
			Utils.drawIconText(dc,heart_rate,cx,y,text_color,heart_icon);
		}else if(info_top == 7){
			var sun_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				if(sunset){
					sun_icon = MoonPhase.getIconSunsetWhite(moment);
				}else{
					sun_icon = sunrise_icon_white;
				}
			}else{
				if(sunset){
					sun_icon = MoonPhase.getIconSunsetBlack(moment);
				}else{
					sun_icon = sunrise_icon_black;
				}
			}
		
			Utils.drawIconText(dc,sunset_sunrise,cx,y,text_color,sun_icon);
		}
			
		if(info_bottom == 0){
			y=dc.getHeight()*0.85;
       		Altimeter.draw(dc,altitude,y,text_color,shade_color);	
		}else if( info_bottom == 1){
			y = dc.getHeight()*5/6;
			Date.drawDate(dc,info_date,cx,y,text_color,date_type);
		}else if(info_bottom ==2){
			y = dc.getHeight()*5/6;
			if(battery_percentage){
				Battery.drawIcon(dc,battery,battery_low,cx,y,text_color,2);
			}else{
				Battery.drawIcon(dc,battery,battery_low,cx,y,text_color,1);
			}
		}else if (info_bottom == 3 && settings.phoneConnected){
			y = dc.getHeight()*5/6;
			PhoneConnected.drawIcon(dc,cx,y,text_color);
		}else if(info_bottom == 4){
			y = dc.getHeight()*5/6;
			var calorie_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				calorie_icon = calorie_icon_white;
			}else{
				calorie_icon = calorie_icon_black;
			}
			Utils.drawIconText(dc,ActivityMonitor.getInfo().calories,cx,y,text_color,calorie_icon);
		}else if(info_bottom == 5){
			y = dc.getHeight()*5/6;
			var step_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				step_icon = step_icon_white;
			}else{
				step_icon = step_icon_black;
			}
			Utils.drawIconText(dc,ActivityMonitor.getInfo().steps,cx,y,text_color,step_icon);
		}else if(info_bottom == 6){
			y = dc.getHeight()*5/6;
			var distance_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				distance_icon = distance_icon_white;
			}else{
				distance_icon = distance_icon_black;
			}
			
			InfoMonitor.drawIconDistance(dc,ActivityMonitor.getInfo().distance,cx,y,text_color,distance_icon);
		}else if(info_bottom == 7){
			y = dc.getHeight()*5/6;
			var heart_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				heart_icon = heart_icon_white;
			}else{
				heart_icon = heart_icon_black;
			}
		
			Utils.drawIconText(dc,heart_rate,cx,y,text_color,heart_icon);
		}else if(info_bottom == 8){
			y = dc.getHeight()*5/6;
			var sun_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				if(sunset){
					sun_icon = MoonPhase.getIconSunsetWhite(moment);
				}else{
					sun_icon = sunrise_icon_white;
				}
			}else{
				if(sunset){
					sun_icon = MoonPhase.getIconSunsetBlack(moment);
				}else{
					sun_icon = sunrise_icon_black;
				}
			}
		
			Utils.drawIconText(dc,sunset_sunrise,cx,y,text_color,sun_icon);
		}
		
		
		if(arc_type<3){
       		
       		if(arc_type == 0){
       			Battery.drawArc(dc,battery,battery_low,cx,cy,getColorArc(),getColorArcLowBattery(),arc_width);
       		}else if (arc_type == 1) {
        		InfoMonitor.drawArcStep(dc,ActivityMonitor.getInfo().steps,ActivityMonitor.getInfo().stepGoal,cx,cy,getColorArc(),getColorArcGoal(),arc_width);
        	}else if (arc_type == 2) {
        		InfoMonitor.drawArcMoveBar(dc,ActivityMonitor.getInfo().moveBarLevel,ActivityMonitor.MOVE_BAR_LEVEL_MAX,cx,cy,getColorArc(),arc_width);
        	} 	
       	} 
       	
       	var alarm_count = settings.alarmCount;
       	
       	if ( alarm_count > 0 && alarm) {
       		var alarm_clock_icon;
			if(bgk_color==Gfx.COLOR_BLACK){
				alarm_clock_icon = alarm_icon_white;
			}else{
				alarm_clock_icon = alarm_icon_black;
			}
       		dc.drawBitmap(cx*2-32-arc_width/2-alarm_clock_icon.getWidth()/2,cy/2-alarm_clock_icon.getHeight()/2,alarm_clock_icon);
       	}
       	      	
    }
            
    function drawHour(dc,info_date,info_bottom,display_second){

        var text_y_hour;
        if(info_bottom==0){
        	text_y_hour = cy-text_height_hour/8;
        }else{
        	text_y_hour = cy;
        }

        var text_width_hour;
        var start_x;
        text_width_hour = text_width_hour_10;
        if(active && display_second){
        	start_x=start_x_active_hour_10;
        }else{
        	start_x=start_x_sleep_hour_10;
		}
        
        var hour_color = getColorHour();
        var minute_color = getColorMinute();
        var second_color = getColorSecond();
        	
		Date.drawHour(dc,[largeFont,smallFont],info_date,[start_x,text_y_hour],[text_width_hour,text_width_point,text_width_minute],[text_height_hour,text_height_second],settings.is24Hour,[hour_color,text_color,minute_color,second_color], display_second);	 
    }
    
    function getColorShade(moment){
        if(shade_color_property == 13){
        	var duration = new Time.Duration(minute_change_color*60);
       		if(time_color == null || moment.compare(time_color.add(duration))>=0){
        		var color = Math.rand()%11+1;
        		while(color == change_color){
        			color = Math.rand()%11+1;
        		}
        		change_color = color;	        		
        		time_color=moment;
        	}
        	return getColor(change_color,Gfx.COLOR_DK_RED);
        }else{
       	 	return getColor(shade_color_property,Gfx.COLOR_DK_RED);
        }
    }
    
    function getColorHour(){
        return getColor(hour_color_property,text_color);
    }
 
    function getColorMinute(){
        return getColor(minute_color_property,shade_color);
    }
        
    function getColorSecond(){
        return getColor(second_color_property,text_color);
    }
            
    function getBackgroundColor(){
        if(bgk_color_property == 0){
        	return Gfx.COLOR_BLACK;
        }else{
        	return Gfx.COLOR_WHITE;
        }
    }
        
    function getColorArc(){
        return getColor(arc_color_property,text_color);
    }
    
    function getColorArcLowBattery(){
        return getColor(arc_color_low_battery_property,Gfx.COLOR_RED);
    }
    
    function getColorArcGoal(){
        return getColor(arc_color_goal_property,Gfx.COLOR_GREEN);
    }
    
   	function getColor(color_property, color_default){
        if (color_property == 1) {
        	return Gfx.COLOR_BLUE;
        }else if (color_property == 2) {
        	return Gfx.COLOR_DK_BLUE;
        }else if (color_property == 3) {
        	return Gfx.COLOR_GREEN;
        }else if (color_property == 4) {
        	return Gfx.COLOR_DK_GREEN;
        }else if (color_property == 5) {
        	return Gfx.COLOR_LT_GRAY;
        }else if (color_property == 6) {
        	return Gfx.COLOR_DK_GRAY;
        }else if (color_property == 7) {
        	return Gfx.COLOR_ORANGE;
        }else if (color_property == 8) {
        	return Gfx.COLOR_PINK;
        }else if (color_property == 9) {
        	return Gfx.COLOR_PURPLE;
        }else if (color_property == 10) {
        	return Gfx.COLOR_RED;
        }else if (color_property == 11) {
        	return Gfx.COLOR_DK_RED;
        }else if (color_property == 12) {
        	return Gfx.COLOR_YELLOW;
        }else if (color_property == 13) {
        	return shade_color;
        }else if (color_property == 14) {
        	return text_color;
        }
        return color_default;
    } 
     
    function getMoment(now,what) {
		return SunCalc.calculate(now, lastLoc[0], lastLoc[1], what);
	}
	
	function momentToString(moment) {

		if (moment == null) {
			return "--:--";
		}

   		var tinfo = Time.Gregorian.info(new Time.Moment(moment.value() + 30), Time.FORMAT_SHORT);
		var text;
		if (settings.is24Hour) {
			text = tinfo.hour.format("%02d") + ":" + tinfo.min.format("%02d");
		} else {
			var hour = tinfo.hour % 12;
			if (hour == 0) {
				hour = 12;
			}
			text = hour.format("%02d") + ":" + tinfo.min.format("%02d");
			
			if (tinfo.hour < 12 || tinfo.hour == 24) {
				text = text + " AM";
			} else {
				text = text + " PM";
			}
		}

		return text;
	}
  
}
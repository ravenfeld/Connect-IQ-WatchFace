using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class WatchFaceApp extends App.AppBase{
	var view;
    function initialize() {
        AppBase.initialize();
    }

    function onStart(state) {
    }

    function onStop(state) {
    }

    function getInitialView() {
        view = new WatchFaceView();
		return [ view ];
    }

    function onSettingsChanged(){
        view.initialize();
    }
}